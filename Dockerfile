# This is an auto generated Dockerfile for ros:desktop-full
# generated from docker_images_ros2/create_ros_image.Dockerfile.em
FROM osrf/ros:humble-desktop-jammy
ENV DISPLAY $DISPLAY
# install ros2 packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    ros-humble-desktop-full=0.10.0-1* \
    && rm -rf /var/lib/apt/lists/*
RUN apt update -y && apt upgrade -y
RUN apt install software-properties-common -y
RUN add-apt-repository universe
RUN apt install ros-humble-navigation2 -y
RUN apt install ros-humble-nav2-bringup -y
RUN apt install ros-humble-turtlebot3-gazebo -y
RUN apt install ros-humble-robot-localization -y
RUN apt install pip -y
RUN pip install ros2-numpy
RUN apt install ros-humble-ros2bag ros-humble-rosbag2-storage-default-plugins libfuse2 -y
RUN apt install python3-colcon-common-extensions -y
RUN apt install wget -y
RUN apt install fuse -y
# get Rust
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y
RUN pip3 install opengen
ENV PATH="/root/.cargo/bin:${PATH}"
RUN echo 'source $HOME/.cargo/env' >> $HOME/.bashrc
RUN cargo install cargo-outdated
RUN cargo install maturin
RUN cargo install evcxr_repl
RUN wget https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage
RUN nvim.appimage
RUN git clone https://github.com/neovide/neovide
WORKDIR /neovide
RUN cargo build --release
WORKDIR ../
RUN pip3 install --user --upgrade git+https://github.com/colcon/colcon-cargo.git
RUN pip install colcon-meson
RUN pip install -U colcon-gradle
RUN echo "eval '$(register-python-argcomplete3 ros2)'" >> $HOME/.bashrc
RUN echo "eval '$(register-python-argcomplete3 colcon)'" >> $HOME/.bashrc
VOLUME /ros_ws
VOLUME /microros_ws
VOLUME /ros_debug_ws
VOLUME /ros_ws/src
#installation of ros2 contro
