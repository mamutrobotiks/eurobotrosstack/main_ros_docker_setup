# main_ros_docker_setup
Main docker setup we use for our robots. As it's now primarily designed to 
be used on the robots themselves, it contains all the packages needed for 
the robots to work properly.

## Dockerfile
we use a custom osrf/ros:humble-desktop-jammy image, which includes all the 
packages we want to run on the robot. since our robots are capable of graphical 
processing, some ros2-specific graphical tools are included. To add new packages, 
you need to go to a new line and start with "RUN" as the bash command that you 
want.

## compose.yaml
A sample docker compose.yaml to launch the resulting image. I haven't used it 

## buildscript.sh
will create the image and give it a name to start with.

## entrypoint.sh
helps to start the container with a lot of privileges, so that graphical input 
and output works, and so that the container can communicate with the underlying 
OS. Use with care, and don't use this setup on an open network at all.
f you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
